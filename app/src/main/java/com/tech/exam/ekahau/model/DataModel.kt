package com.tech.exam.ekahau.model

import java.util.*
import kotlin.collections.HashMap

class TreeNode {

    var id: String? = null
    var type: String? = null
    var decayScore: Int = 0
    var children = mutableListOf<TreeNode>()
    var updateScore = 0

    constructor(id: String, type: String, decayScore: Int) {
        this.id = id
        this.type = type
        this.decayScore = decayScore
        this.children = mutableListOf()
    }

    constructor(id: String, type: String, decayScore: Int, children: MutableList<TreeNode>) {
        this.id = id
        this.type = type
        this.decayScore = decayScore
        this.children = children
    }


    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false
        else {
            if (other is TreeNode) {
                if (other.id == id && other.type == type) {
                    return true
                }
            }
        }
        return false
    }

    override fun hashCode(): Int {
        return if (type == "APM")
            (if (id != null) id.hashCode() else 0) * 31 +
                (if (type != null) type.hashCode() else 0) * 31
        else
            super.hashCode()
    }

    override fun toString(): String {
        return "$id:$decayScore"
    }
}

data class APMHistory(val apId: String, val rId: String, val apmId: String)

data class Recorder(val apMap: HashMap<String, TreeNode>, val temporaryStack: Stack<String>, val apmHistory: HashMap<String, APMHistory>, val disputedAPM: ArrayList<APMHistory>) {
}
