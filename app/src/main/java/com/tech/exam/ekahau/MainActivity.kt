package com.tech.exam.ekahau

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.grouping_exam.DataSource
import com.tech.exam.ekahau.model.*
import java.util.*

class MainActivity: AppCompatActivity() {

    private val recorder = Recorder(hashMapOf(), Stack<String>(), hashMapOf(), arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataSource.emit()
           .subscribe{
               Log.d("Logger", "pre-process $it")
               processData(it)
           }
    }

    private fun processData(it: List<String>?) {
        val startTime = System.currentTimeMillis()
        recorder.parseData(it)
        recorder.validate()
        recorder.print()
        Log.d("Logger", "finished by ${System.currentTimeMillis() - startTime} ms")
    }

    private fun Recorder.parseData(it: List<String>?) {
        this.temporaryStack.clear()
        it?.forEach { item ->
            if (item.contains("APM") || item.contains("R")) {
                this.temporaryStack.push(item)
            }
            else if (item.contains("AP")) {
                var ap = this.apMap[item]
                if (ap == null) ap = TreeNode(item, "AP", 0, mutableListOf())
                val stack = this.temporaryStack
                var r: TreeNode? = null
                var rIndex = -1
                while (stack.isNotEmpty()) {
                    val pop = stack.pop()
                    if (pop.contains("R")) {
                        ap.children.forEachIndexed radioLoop@ { index, node ->
                            if (node.id == pop) {
                                r = node
                                rIndex = index
                                return@radioLoop
                            }
                        }
                        if (r == null) {
                            rIndex = -1
                            r = TreeNode(pop, "R", 0, mutableListOf())
                        }
                    }
                    else if (pop.contains("APM")) {
                        r?.let { radio ->
                            val apm = radio.children.singleOrNull { it.id == pop }
                            val apmHistory = APMHistory(ap.id ?: "", radio.id ?: "", pop)
                            val existingAPM = this.apmHistory[pop]
                            existingAPM?.let {
                                if (it.apId != apmHistory.apId || it.rId != apmHistory.rId || it.apmId != apmHistory.apmId) {
                                    this.disputedAPM.add(apmHistory)
                                }
                            } ?: this.apmHistory.put(pop, apmHistory)

                            if (apm == null) {
                                radio.children.add(TreeNode(pop, "APM", 0))
                            }
                            else {
                                radio.children.forEachIndexed { index, node ->
                                    if (node.id == pop) {
                                        node.updateScore++
                                        node.decayScore = 0
                                        radio.children[index] = node
                                        radio.decayScore = 0
                                        radio.updateScore++
                                        ap.decayScore = 0
                                        ap.updateScore++
                                    }
                                }
                            }
                            if (stack.isEmpty() || stack.peek().contains("R")) {
                                if (rIndex > -1) {
                                    ap.children[rIndex] = radio
                                }
                                else {
                                    ap.children.add(radio)
                                }
                                r = null
                                rIndex = -1
                            }
                        }
                    }
                }
                if (ap.children.isNotEmpty()) {
                    this.apMap[item] = ap
                }
            }
        }
    }

    private fun Recorder.validate() {
        val correctedAPMIndex = arrayListOf<APMHistory>()
        this.disputedAPM.forEach { disputed ->
            val existingAPM = recorder.apmHistory[disputed.apmId]
            existingAPM?.let { existing ->
                val existingNode = this.apMap[existing.apId]
                val disputedNode  = this.apMap[disputed.apId]
                if (existingNode != null && disputedNode != null) {
                    val existingRNode = existingNode.children.find { it.id == existing.rId}
                    val disputedRNode = disputedNode.children.find { it.id == disputed.rId}
                    if (existingRNode != null && disputedRNode != null) {
                        val merged = existingRNode.children.union(disputedRNode.children)
                        if (existingRNode.updateScore >= disputedRNode.updateScore && existingRNode.decayScore >= disputedRNode.decayScore) {
                            merged.forEach {
                                it.decayScore = 0
                                it.updateScore++
                            }
                            existingRNode.children = merged.toMutableList()
                            existingRNode.decayScore = 0
                            existingRNode.updateScore++
                            existingNode.decayScore = 0
                            existingNode.updateScore++
                            disputedNode.children.remove(disputedRNode)
                            if (disputedNode.children.isEmpty()) {
                                this.apMap.remove(disputed.apId)
                            }
                        }
                        else {
                            merged.forEach {
                                it.decayScore = 0
                                it.updateScore++
                                apmHistory[it.id ?: ""] = APMHistory(disputedNode.id ?: "", disputedRNode.id ?: "", it.id ?: "")
                            }
                            disputedRNode.children = merged.toMutableList()
                            disputedRNode.decayScore = 0
                            disputedNode.updateScore++
                            disputedNode.decayScore = 0
                            disputedNode.updateScore++
                            existingNode.children.remove(existingRNode)
                            if (existingNode.children.isEmpty()) {
                                this.apMap.remove(existing.apId)
                            }
                        }
                    }
                }
            }
            correctedAPMIndex.add(disputed)
        }
        correctedAPMIndex.forEach { disputedAPM.remove(it) }
    }

    private fun Recorder.print() {
        val list = LinkedList<String>()
        this.apMap.toSortedMap().forEach {
            val node = it.value
            node.children.forEachIndexed { index, rNode ->
                rNode.children.forEachIndexed { index, apmNode ->
                    list.add("${apmNode.id}:${apmNode.decayScore}")
                    apmNode.decayScore++
                    rNode.children[index] = apmNode
                }
                list.add("${rNode.id}:${rNode.decayScore}")
                rNode.decayScore++
                node.children[index] = rNode
            }
            list.add("${node.id}:${node.decayScore}")
            node.decayScore++
            apMap[it.key] = node
        }
        Log.d("Logger", "printing $list")
    }

    override fun onResume() {
        super.onResume()
        recorder.temporaryStack.clear()
        recorder.apMap.clear()
        recorder.disputedAPM.clear()
    }
}



